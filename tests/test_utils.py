import pytest
import sys, os, tempfile, base64, hashlib
import usexp
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from datashards.stores import MemoryStore, FileStore, RemoteMagencStore
from datashards.client import Client
from datashards.utils import store, client

def test_store_types():
    assert isinstance(store('memory://') , MemoryStore)
    assert isinstance(store('file:///'), FileStore)
    assert isinstance(store('magenc+http://localhost:9000'), RemoteMagencStore)
    with pytest.raises(ValueError):
        store('blahstore://')

def test_client():
    cl = client('memory://')
    assert isinstance(cl, Client)
    assert isinstance(cl.store, MemoryStore)
