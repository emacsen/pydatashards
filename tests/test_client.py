import pytest
import sys, os, tempfile, base64, hashlib
import usexp
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from datashards.stores.memorystore import MemoryStore
import datashards.client

# Some constants to make things easier

SMALL_FILENAME = 'tests/gnu-head.png'
LARGE_FILENAME = 'tests/home_goblin.png'

SMALL_STATIC_DIGEST = "ESFNzYxgM5dgNLnqj-VYNAVVNcOek45vLc8XBlEwdK4="
SMALL_STATIC_XT = f"urn:sha256d:{SMALL_STATIC_DIGEST}"
SMALL_STATIC_URN = f"idsc:p0.ESFNzYxgM5dgNLnqj-VYNAVVNcOek45vLc8XBlEwdK4=.YWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWE"

LARGE_STATIC_DIGEST = "CoVzEmewst1HXcmexaVzQcpSLBnTvM8RiLRtuGV5aIs="
LARGE_STATIC_XT = f"urn:sha256d:{LARGE_STATIC_DIGEST}"
LARGE_STATIC_URN = f"idsc:p0.CoVzEmewst1HXcmexaVzQcpSLBnTvM8RiLRtuGV5aIs=.YWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWFhYWE"


def testkey():
    return b'a' * 32

@pytest.fixture(scope="module")
def client():
    store = MemoryStore()
    client = datashards.client.Client(store) 
    return client

def test_client_upload_small(client):
    fd = open(SMALL_FILENAME, 'rb')
    res = client.upload(fd, keyfun=testkey)
    fd.close()
    assert res == SMALL_STATIC_URN

def test_store_has_data(client):
    res = client.store.get(SMALL_STATIC_XT)
    assert res != None

def test_make_manifest():
    xts = [b'abc', b'def', b'ghi']
    # 31000 is less than 32k
    size = 31000
    res = datashards.client.make_manifest(xts, size)
    l = usexp.loadb(res)
    assert isinstance(l, list)
    assert l[0] == b'manifest'
    assert l[1] == b'31000'
    assert l[2] == xts[0]

def test_client_download_small(client):
    output = tempfile.TemporaryFile()
    client.download(SMALL_STATIC_URN, output)


def test_client_upload_large(client):
    fd = open(LARGE_FILENAME, 'rb')
    res = client.upload(fd, keyfun=testkey)
    fd.close()
    assert res == LARGE_STATIC_URN

def test_client_read_manifest(client):
    key = testkey()
    encrypted_manifest = client.store.get(LARGE_STATIC_XT)
    raw_manifest = datashards.client.decrypt_shard_entry(encrypted_manifest, key)
    manifest = usexp.loadb(raw_manifest)
    assert len(manifest) == 5

def test_client_download_large(client):
    output = tempfile.TemporaryFile()
    client.download(LARGE_STATIC_URN, output)

def test_client_small_full(client):
    SMALL_FILE_PATH = 'tests/gnu-head.png'
    fd = open(SMALL_FILENAME, 'rb')
    # This URN is unpredictable since the key is random
    urn = client.upload(fd)
    fd.close()
    output = tempfile.TemporaryFile()
    client.download(urn, output)

def test_oversized_manifest():
    # This is completely bogus
    xts = [SMALL_STATIC_URN] * 500
    with pytest.raises(NotImplementedError):
        datashards.client.make_manifest(xts, 100000)

def test_unsupported_methods():
    class FakeStore():
        pass
    fs = FakeStore()
    badship = datashards.client.Client(fs)
    with pytest.raises(NotImplementedError):
        fd = open(SMALL_FILENAME, 'rb')
        badship.upload(fd)
    with pytest.raises(NotImplementedError):
        output = tempfile.TemporaryFile()
        badship.download(SMALL_STATIC_URN, output)

 
